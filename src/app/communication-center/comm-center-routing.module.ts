import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { CommunicationComponent } from './comm-center.component';

@NgModule({
    imports: [
        RouterModule.forChild([{ path: '', component: CommunicationComponent}])
    ],
    exports: [RouterModule]
})
export class CommunicationCenterRoutingModule {}