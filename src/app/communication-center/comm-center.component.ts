import { Component } from '@angular/core';

@Component({
    templateUrl: './comm-center.component.html',
    styleUrls: ['./comm-center.component.css']
})
export class CommunicationComponent {
    constructor() {}
    public colNamesProperty = [
        {
            colName: 'Pkg #',
            fieldName: 'itemnumber',
        },
        {
            colName: 'Package Name',
            fieldName: 'itemname',
        },
        {
            colName: 'Package Type',
            fieldName: 'type'
        },
        {
            colName: 'Package Type',
            fieldName: 'template'
        },
        {
            colName: 'Package Type',
            fieldName: 'Date'
        },
        {
            colName: 'Package Type',
            fieldName: 'total'
        },
        {
            colName: 'Package Type',
            fieldName: 'status'
        }
    ];
    public packages = [
        {
            itemnumber: '1',
            itemname: 'Save the Date',
            type: 'Email',
            template:'Auction Open Email',
            Date:'3/18/2017 10:14 AM',
            total:'600 / 500',
            status:'Sent'
        },
        {
            itemnumber: '2',
            itemname: 'Save the Date',
            type: 'Email',
            template:'Auction Open Email',
            Date:'3/18/2017 10:14 AM',
            total:'450',
            status:'Scheduled'
        },
        {
            itemnumber: '3',
            itemname: 'Thank You',
            type: 'Letter',
            template:'Donor Thanks',
            Date:'3/18/2017 10:14 AM',
            total:'300',
            status:'Draft'
        },
        {
            itemnumber: '4',
            itemname: 'Thank You',
            type: 'Letter',
            template:'Donor Thanks',
            Date:'3/18/2017 10:14 AM',
            total:'430',
            status:'Sent'
        },
        {
            itemnumber: '5',
            itemname: 'Win the item',
            type: 'Text',
            template:'Highest Bid',
            Date:'3/18/2017 10:14 AM',
            total:'700',
            status:'Sent'
        }
    ];    
}