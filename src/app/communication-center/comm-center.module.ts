import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GridModule } from '@progress/kendo-angular-grid';
import { CommunicationComponent } from './comm-center.component';
import { CommunicationCenterRoutingModule } from './comm-center-routing.module';

@NgModule({
    declarations: [CommunicationComponent],
    imports: [CommonModule, CommunicationCenterRoutingModule, GridModule]
})
export class CommunicationCenterModule {}