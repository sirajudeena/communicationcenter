var HtmlWebpackPlugin = require('html-webpack-plugin');
var ExtractTextPlugin = require('extract-text-webpack-plugin');

module.exports = {
    entry: './src/app/main.ts',
    resolve: {
        extensions: ['.js', '.ts']
    },
    module: {
        rules: [
            {
                test: /\.html$/,
                loaders: ['html-loader']
            },
            {
                test: /\.css$/,
                loaders: ['raw-loader']
            },
            { // sass / scss loader for webpack
                test: /\.(sass|scss)$/,
                loader: ExtractTextPlugin.extract(['css-loader', 'sass-loader'])
            }
        ],
        exprContextCritical: false
    },
    plugins: [
        // new ExtractTextPlugin({ // define where to save the file
        //     filename: 'dist/[name].bundle.css',
        //     allChunks: true,
        //   }),
        
        new HtmlWebpackPlugin({
            template: 'src/index.html'
        })
    ]
};